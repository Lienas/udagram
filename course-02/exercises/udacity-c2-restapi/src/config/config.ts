export const config = {
  "dev": {
    "username": process.env.POSTGRESS_USERNAME, //udagramlucasdev
    "password":  process.env.POSTGRESS_PASSWORD,
    "database": process.env.POSTGRESS_DB,
    "host": process.env.POSTGRESS_HOST,  //"udagramlucasdev2.c8zodkyyi664.eu-central-1.rds.amazonaws.com",
    "dialect": process.env.DIALECT, //"postgres",
    "aws_region": process.env.AWS_REGION,  // "eu-central-1",
    "aws_profile": process.env.AWS_PROFILE,// default
    "aws_media_bucket":process.env.AWS_MEDIA_BUCKET   //"udagram-lucas-dev"
  },
  "prod": {
    "username": "",
    "password": "",
    "database": "udagram_prod",
    "host": "",
    "dialect": "postgres"
  }
}
